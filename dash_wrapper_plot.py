# -*- coding: utf-8 -*-
import dash
import dash_core_components as dcc
import dash_html_components as html
import pandas as pd
import numpy as np


def parse_data(data: np.ndarray, max_num_rows: int = None):
    """
    :param data: [np.ndarray] [x1, y1, x2, y2, ... ] in sequence
    :param max_num_rows: [int] maximum number of rows to be read
    :return: df[pd.DataFrame]
    """
    if data.shape(1) < 2:
        raise ValueError("data should have at least 2 elements")

    if data.shape(0) > max_num_rows:
        data = data[0:max_num_rows, :]
    df = pd.DataFrame(data)
    return df


def parse_datafile(filename, max_num_rows: int = None):
    """
    :param filename: [str] name of csv file, first line becomes header
    :param max_num_rows: [int] maximum number of rows to be read
    :return: df[pd.DataFrame]
    """
    df = pd.read_csv(filename, header=0, nrows=max_num_rows)
    if df.shape[1] < 2:
        raise ValueError("Too few column in the csv file")

    return df


class DashPlot2DInterface:
    """
    Attributes
    ----------
    data: [pd.DataFrame]
    title: [html.H1]
    graph: [dcc.graph]
    id_prefix: [str]
    """
    def __init__(
            self,
            data: np.ndarray = None,
            filename: str = "",
            title="",
            max_num_rows: int = None,
            id_prefix: str = "",
            plot_dim: (int, int) = (3, 3),
            data_groups: list = []
    ):
        if data is None and not filename:
            raise ValueError("data or data file is required")

        if data is not None:
            self.data = parse_data(data, max_num_rows=max_num_rows)
        else:
            self.data = parse_datafile(filename, max_num_rows=max_num_rows)

        if title:
            self.title = html.H1(children=title, style={'textAlign': 'center'})
        else:
            self.title = None

        self.id = id_prefix
        self.plot_dim = plot_dim
        self.data_groups = data_groups

        self.dropdown_options = []
        for i, column_name in enumerate(self.data.columns):
            self.dropdown_options.append({'label': column_name, 'value': i})

        self.dash_plots = []
        for i in range(self.plot_dim[0]*self.plot_dim[1]):
            dash_plot_id = self.id + '{}'.format(i)
            if len(self.data_groups) > i:
                dash_plot = DashPlot2D(
                    dash_plot_id,
                    self.dropdown_options,
                    self.data,
                    initial_data=self.data_groups[i]
                )
            else:
                if i < len(self.dropdown_options)-1:
                    dash_plot = DashPlot2D(
                        dash_plot_id,
                        self.dropdown_options,
                        self.data,
                        initial_data=[i+1]
                    )
                else:
                    dash_plot = DashPlot2D(
                        dash_plot_id,
                        self.dropdown_options,
                        self.data,
                        initial_data=[len(self.dropdown_options)-1]
                    )
            self.dash_plots.append(dash_plot)

    def draw_figure(self):
        plot_height = '{0:d}%'.format(int(100/self.plot_dim[0] - 6))
        plot_width = '{0:d}%'.format(int(100 / self.plot_dim[1] - 6))

        div_list = []
        for j in range(self.plot_dim[1]):
            for i in range(self.plot_dim[0]):
                plot = self.dash_plots[j*self.plot_dim[0]+i]
                div_graph = plot.draw_graph()
                div_dropdown = plot.draw_dropdown()

                div_list.append(
                    html.Div(
                        children=[div_graph, div_dropdown],
                        style={
                            'width': plot_width,
                            'height': plot_height,
                            'display': 'inline-block',
                            'vertical-align': 'top',
                            'border': '1px solid',
                            'marginLeft': '5%',
                            'marginTop': '5%'
                        }
                    )
                )

        return html.Div(children=div_list)


class DashPlot2D:
    def __init__(self, id_prefix: str, dropdown_options, data, initial_data: list = []):
        self.id = id_prefix
        self.dropdown_options = dropdown_options
        self.data = data

        self.x_axis_dropdown = dcc.Dropdown(
            options=self.dropdown_options,
            value=0,
            id=self.id + "_x_dropdown"
        )

        self.y_axis_dropdown = dcc.Dropdown(
            options=self.dropdown_options,
            value=initial_data,
            multi=True,
            id=self.id + "_y_dropdown"
        )

        self.graph = dcc.Graph(
            id=self.id + "_graph",
            figure=self.update_graph(self.x_axis_dropdown.value, self.y_axis_dropdown.value)
        )

    def graph_id(self):
        return self.graph.id

    def x_axis_dropdown_id(self):
        return self.x_axis_dropdown.id

    def y_axis_dropdown_id(self):
        return self.y_axis_dropdown.id

    def update_graph(self, x_axis, y_axis):
        data = []
        for d in y_axis:
            datum = {
                    'x': self.data.iloc[:, x_axis],
                    'y': self.data.iloc[:, d],
                    'type': 'scatter',
                    'name': self.dropdown_options[d]['label']
                }
            data.append(datum)

        layout = {}
        if len(y_axis) > 1:
            layout = {
                'title': 'variables',
                'xaxis': {
                    'title': self.dropdown_options[x_axis]['label']
                }
            }
        if len(y_axis) == 1:
            layout = {
                'title': self.dropdown_options[y_axis[0]]['label'],
                'xaxis': {
                    'title': self.dropdown_options[x_axis]['label']
                },
                'yaxis': {
                    'title': self.dropdown_options[y_axis[0]]['label']
                }
            }

        figure = {'data': data, 'layout': layout}
        return figure

    def draw_dropdown(self):
        div_dropdown = html.Div(
            children=[
                # html.H6("Plot Settings"),
                html.Div(
                    children=[
                        html.Label("x Axis"),
                        self.x_axis_dropdown
                    ],
                    style={'width': '40%', 'display': 'inline-block', 'marginLeft': '5%', 'marginRight': '5%'}
                ),
                html.Div(
                    children=[
                        html.Label("y Axis"),
                        self.y_axis_dropdown
                    ],
                    style={'width': '40%', 'display': 'inline-block', 'marginLeft': '5%'}
                )
            ],
            style={
                'width': '90%',
                'display': 'inline-block',
                'vertical-align': 'top',
                'marginLeft': '5%',
                'marginRight': '5%',
                'marginBottom': '5%'
            }
        )
        return div_dropdown

    def draw_graph(self):
        div_graph = html.Div(
            children=[
                # html.H6("Graphs"),
                self.graph
            ],
            style={
                'width': '100%',
                'height': '100%',
                'display': 'inline-block',
                'vertical-align': 'top',
            }
        )
        return div_graph

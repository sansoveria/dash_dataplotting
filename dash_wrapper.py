# -*- coding: utf-8 -*-
import dash
from dash_wrapper_plot import DashPlot2DInterface as plot2D
import dash_html_components as html
from dash.dependencies import Input, Output

external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

plot_dim = (4, 2)
data_groups = [[1, 7, 13]]
plt = plot2D(filename="test_data.csv", max_num_rows=None, id_prefix="my_graph", plot_dim=plot_dim, data_groups=data_groups)
app = dash.Dash(__name__, external_stylesheets=external_stylesheets)
app.layout = html.Div(
    children=[
        html.H1("Test"),
        plt.draw_figure()
    ]
    )

for j in range(plot_dim[1]):
    for i in range(plot_dim[0]):
        plot = plt.dash_plots[j*plot_dim[0]+i]
        @app.callback(
            Output(component_id=plot.graph_id(), component_property='figure'),
            [
                Input(component_id=plot.x_axis_dropdown_id(), component_property='value'),
                Input(component_id=plot.y_axis_dropdown_id(), component_property='value')
            ]
        )
        def axis_dropdown_callback(x_axis, y_axis):
            figure = plot.update_graph(x_axis, y_axis)
            return figure


if __name__ == '__main__':
    app.run_server(debug=True)